<!doctype html>
<html>
<head>
    <style>
        body
        {
            margin: 0;
            padding: 0;
            overflow-y: scroll;
            overflow-x: hidden;
            width: 100%;

        }
        .array{
            padding: 5px;
            margin-left: 10px;
            position: relative;
            width: 40%;
            height: 100%;
            overflow: hidden;
        }



    </style>

    <meta name="Description" content="این یک تمرین واکنشگرایی است">
    <link rel="icon" href="images/Logo.png" type="image/png" sizes="16x16">
    <link rel="stylesheet" media="screen and (min-width: 901px)" href="css/media.css">
    <link rel="stylesheet" media="screen and (max-width: 900px)" href="css/between.css">
    <link rel="stylesheet" media="screen and (max-width: 600px)" href="css/small.css">
    <meta charset="utf-8">
    <title>امتحان میان ترم - درست کردن یک سایت ساده</title>
</head>

<body>
<?php include "include/header"
?>

<?php include "include/left"
?>




<div class="main1">
<div class="array">
    <?php
$names = array("mahmoud", "reza" , 54);
    echo "<br>";
var_dump($names);
    echo "<br>";
echo $names[2];
    echo "<br>";
echo count($names);
$array = array(
        "bar" => "jar",
        "jar" => "kar"
);
    echo "<br>";
var_dump($array);
    echo "<br>";
$array1= array(
        "jar" => "kar",
         57 => 47,
         "multi" => array(
                 "demolition" => array(
                         "array" => "foo"
                 )
         )
);
var_dump($array1);
    echo "<br>";
var_dump($array1["jar"]);
    echo "<br>";
var_dump($array1[57]);
    echo "<br>";
var_dump($array1["multi"]["demolition"]["array"]);


?>


</div>




</div>






<?php include "include/right"
?>

<?php include "include/footer"
?>

</body>
</html>
