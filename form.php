<!doctype html>
<html>
<head>
    <style>
        body
        {
            margin: 0;
            padding: 0;
            overflow-y: scroll;
            overflow-x: hidden;
            width: 100%;

        }
   .container{
       direction: rtl;
       overflow: hidden;

   }






    </style>

    <meta name="Description" content="این یک تمرین واکنشگرایی است">
    <link rel="icon" href="images/Logo.png" type="image/png" sizes="16x16">
    <link rel="stylesheet" media="screen and (min-width: 901px)" href="css/media.css">
    <link rel="stylesheet" media="screen and (max-width: 900px)" href="css/between.css">
    <link rel="stylesheet" media="screen and (max-width: 600px)" href="css/small.css">
    <link rel="stylesheet"  href="css/form.css">
    <meta charset="utf-8">
    <title>امتحان میان ترم - درست کردن یک سایت ساده</title>
</head>

<body>
<?php include "include/header"
    ?>

<?php include "include/left"
?>



<div class="main1">


    <div class="container" >
        <form action="destination.php" method="post">
            <div class="field">
                <label for="name">نام </label><input type="text" id="name" placeholder="نام" name="name">
            </div>
            <div class="field">
                <label for="l-name">نام خانوادگی </label><input type="text" id="l-name" placeholder="نام خانوادگی" name="l-name">
            </div>
            <div class="field">
                <label>جنسیت</label>
                <span><input  type="radio" name="gender" value="مرد"><label>مرد</label></span>
                <span><input  type="radio" name="gender" value="زن"><label>زن</label></span>
            </div>
            <div class="field">
                <label>مدرک تحصیلی</label>
                <select name="education" >
                    <option value="diplom">دیپلم</option>
                    <option value="Foghe diplom">فوق دیپلم</option>
                    <option value="master">کارشناسی</option>
                    <option value="bachalor master">کارشناسی ارشد</option>
                </select>

            </div>
            <div class="field">
                <label for="add">آدرس</label><input type="text" id="add" placeholder="آدرس" name="adress">
            </div>
            <div id="btn" class="field">
                <input type="reset" value="پاک کردن">
                <input type="submit" value="ارسال">

            </div>
        </form>
    </div>


</div>




<?php include "include/right"
    ?>

<?php include "include/footer"
?>



</body>
</html>
