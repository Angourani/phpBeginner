<!doctype html>
<html>
<head>
    <style>
        body
        {
            margin: 0;
            padding: 0;
            overflow-y: scroll;
            overflow-x: hidden;
            width: 100%;

        }
        .array{
            padding: 5px;
            margin-left: 10px;
            position: relative;
            width: 40%;
            height: 100%;
            overflow: hidden;
        }



    </style>

    <meta name="Description" content="این یک تمرین واکنشگرایی است">
    <link rel="icon" href="images/Logo.png" type="image/png" sizes="16x16">
    <link rel="stylesheet" media="screen and (min-width: 901px)" href="css/media.css">
    <link rel="stylesheet" media="screen and (max-width: 900px)" href="css/between.css">
    <link rel="stylesheet" media="screen and (max-width: 600px)" href="css/small.css">
    <meta charset="utf-8">
    <title>امتحان میان ترم - درست کردن یک سایت ساده</title>
</head>

<body>
<?php include "include/header"
?>

<?php include "include/left"
?>




<div class="main1">
    <div class="array">

        <?php
        echo "<br><br>";
        function get_names()
        {
            return array("ali" , "mari" , "masood");

        }
        list($first, $second , $third) = get_names();
        echo $first;
        echo "<br><br>";
        echo $second;
        echo "<br><br>";
        echo $third;


        function getBestFriend($name = "Mostafa")
        {
            return "My best friend is " . $name;
        }

        echo "<br><br>";
        echo getBestFriend();
        echo "<br><br>";
        echo getBestFriend(null);
        echo getBestFriend("Hosein");
        echo "<br><br>";
        ?>




    </div>




</div>






<?php include "include/right"
?>

<?php include "include/footer"
?>

</body>
</html>
